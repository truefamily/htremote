package com.trueberry.hometheatre.preferences;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.preference.DialogPreference;
import android.util.AttributeSet;
import android.view.View;
import android.widget.NumberPicker;

import com.trueberry.hometheatre.R;

/**
 * Класс для выбора числовых значений в настройках приложения
 */
public class NumberPickerPreference extends DialogPreference {

    private NumberPicker mPicker;
    private int minValue = 0;
    private int maxValue = 50;
    private Integer currentNumber = 0;
    private String measurement = "";
    private Context ctx;

    public NumberPickerPreference(Context context, AttributeSet attrs) {
        super(context, attrs);

        ctx = context;
        Resources res = ctx.getResources();

        for (int i=0; i < attrs.getAttributeCount(); i++) {
            String attr = attrs.getAttributeName(i);
            String val  = attrs.getAttributeValue(i);
            switch (attr) {
                case "minValue":
                    minValue = Integer.valueOf(val);
                    break;
                case "maxValue":
                    maxValue = Integer.valueOf(val);
                    break;
                case "measurement":
                    int measureId = Integer.valueOf(val);
                    measurement = measureId == 1 ? res.getString(R.string.sec) : res.getString(R.string.min);
                    break;
            }
        }
    }

    public NumberPickerPreference(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setPositiveButtonText(android.R.string.ok);
        setNegativeButtonText(android.R.string.cancel);
    }

    @Override
    protected View onCreateDialogView() {
        mPicker = new NumberPicker(getContext());
        mPicker.setMinValue(minValue);
        mPicker.setMaxValue(maxValue);
        mPicker.setValue(currentNumber);
        return mPicker;
    }

    @Override
    protected void onDialogClosed(boolean positiveResult) {
        if (positiveResult) {
            // Нужно, когда пользователь редактирует текст и нажимает ОК
            mPicker.clearFocus();
            setValue(mPicker.getValue());
            setCustomSummary();
        }
    }

    public void setCustomSummary() {
        Resources res = ctx.getResources();
        this.setSummary(res.getString(R.string.currentValue) + " " + String.valueOf(currentNumber) + " " + measurement);
    }


    @Override
    protected void onSetInitialValue(boolean restoreValue, Object defaultValue) {
        setValue(restoreValue ? getPersistedInt(currentNumber) : (Integer) defaultValue);
    }

    public Integer getCurrentNumber() {
        return currentNumber;
    }

    public String getMeasurement() {
        return measurement;
    }

    public void setValue(int value) {
        if (shouldPersist()) {
            persistInt(value);
        }

        if (value != currentNumber) {
            currentNumber = value;
            notifyChanged();
        }
    }

    @Override
    protected Object onGetDefaultValue(TypedArray a, int index) {
        return a.getInt(index, 0);
    }
}