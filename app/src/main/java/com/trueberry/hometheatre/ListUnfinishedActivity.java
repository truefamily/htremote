package com.trueberry.hometheatre;

import com.trueberry.hometheatre.utils.ActivityListType;
import com.trueberry.hometheatre.utils.Command;

/**
 * Created by root on 06.08.2015.
 * Список недосмотренных видео.
 */
public class ListUnfinishedActivity extends ListActivity {

    public ListUnfinishedActivity() {
        getListCommand = Command.NotFinishedList;
        type = "unfinished";
        listType = ActivityListType.NOT_FINISHED_LIST;
    }
}
