package com.trueberry.hometheatre;

import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.beardedhen.androidbootstrap.BootstrapButton;
import com.beardedhen.androidbootstrap.BootstrapLabel;
import com.beardedhen.androidbootstrap.TypefaceProvider;
import com.beardedhen.androidbootstrap.api.attributes.BootstrapBrand;
import com.beardedhen.androidbootstrap.api.defaults.DefaultBootstrapBrand;
import com.github.lguipeng.library.animcheckbox.AnimCheckBox;
import com.trueberry.hometheatre.helpers.CurrentVideoInfo;
import com.trueberry.hometheatre.helpers.SingleToast;
import com.trueberry.hometheatre.receivers.VideoInfoService;
import com.trueberry.hometheatre.receivers.VideoInfoServiceCallbacks;
import com.trueberry.hometheatre.utils.Command;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Автор: root, дата: 06.08.2015.
 */
public class ControlActivity extends AppCompatActivity implements VideoInfoServiceCallbacks {
    // Длина заголовка - названия видео, в символах
    public static int TITLE_LENGTH_LIMIT = 40;

    private VideoInfoService videoInfoService;
    private boolean bound = false;

    private SharedPreferences prefs = null;

    SwipeRefreshLayout mSwipeRefreshLayout;
    Connector conn;
    TextView checkBoxText, curTime, totalTime;
    BootstrapLabel fileNameTV;
    BootstrapButton closeAndDelete, closeAndSave, close,
            incVol, decVol, forward, backward, fastForward,
            fastBackward, muteBtn, fullScreen, sound;
    ProgressBar pBar;
    FloatingActionButton playPauseBtn;
    Drawable playImg, pauseImg;
    AnimCheckBox sysVolumeCheckBox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TypefaceProvider.registerDefaultIconSets();
        setContentView(R.layout.control);

        prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        conn = new Connector(getApplicationContext());
        playImg = getApplicationContext().getResources().getDrawable( R.drawable.play_icon);
        pauseImg = getApplicationContext().getResources().getDrawable( R.drawable.pause_icon);

        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.control_swipe_refresh_layout);
        pBar = (ProgressBar) findViewById(R.id.progressBar);
        curTime = (TextView) findViewById(R.id.currentPositionText);
        totalTime = (TextView) findViewById(R.id.totalTimeText);

        sysVolumeCheckBox = (AnimCheckBox) findViewById(R.id.checkBoxVolume);
        checkBoxText = (TextView) findViewById(R.id.checkBoxVolumeText);
        checkBoxText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sysVolumeCheckBox.performClick();
            }
        });

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshContent();
            }
        });

        fileNameTV = (BootstrapLabel) findViewById(R.id.fileNameTextView);

        playPauseBtn = (FloatingActionButton) findViewById(R.id.playPauseId);
        playPauseBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!((CurrentVideoInfo)getApplicationContext()).isFileInfoExist()) return;
                if (videoInfoService != null) {
                    if ("2".equals(((CurrentVideoInfo)getApplicationContext()).getStatus())) {
                        videoInfoService.stopRequests();
                    } else {
                        videoInfoService.startRequests();
                    }
                }
                playPauseBtn.setImageDrawable("2".equals(((CurrentVideoInfo)getApplicationContext()).getStatus()) ? pauseImg : playImg);
                conn.sendCommandWithoutArgs(Command.PlayPause);
                refreshProgressBar();
                refreshTitle();
            }
        });

        incVol = (BootstrapButton) findViewById(R.id.incVolId);
        incVol.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!((CurrentVideoInfo)getApplicationContext()).isFileInfoExist()) return;
                sendCommandChangeVolume("inc", sysVolumeCheckBox.isChecked());
            }
        });

        decVol = (BootstrapButton) findViewById(R.id.decVolId);
        decVol.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!((CurrentVideoInfo)getApplicationContext()).isFileInfoExist()) return;
                sendCommandChangeVolume("dec", sysVolumeCheckBox.isChecked());
            }
        });

        sound = (BootstrapButton) findViewById(R.id.soundId);
        sound.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                conn.sendCommandWithoutArgs(Command.Sound);

            }
        });

        fullScreen = (BootstrapButton) findViewById(R.id.fullScreenBtn);
        fullScreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!((CurrentVideoInfo)getApplicationContext()).isFileInfoExist()) return;
                conn.sendCommandWithoutArgs(Command.FullScreen);
            }
        });

        closeAndDelete = (BootstrapButton) findViewById(R.id.closeDeleteBtnId);
        closeAndDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!((CurrentVideoInfo)getApplicationContext()).isFileInfoExist()) return;
                AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
                builder.setMessage(getString(R.string.close_and_delete_file))
                        .setPositiveButton(getString(R.string.yes), closeAndDeleteDialogListener)
                        .setNegativeButton(getString(R.string.no), null).show();
            }
        });

        closeAndSave = (BootstrapButton) findViewById(R.id.closeSavePosBtnId);
        closeAndSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!((CurrentVideoInfo)getApplicationContext()).isFileInfoExist()) return;
                conn.sendCommandWithoutArgs(Command.CloseSave);
                ((CurrentVideoInfo)getApplicationContext()).deleteFileInfo();
                Intent intent = new Intent(ControlActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });

        close = (BootstrapButton) findViewById(R.id.closeBtnId);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!((CurrentVideoInfo)getApplicationContext()).isFileInfoExist()) return;
                conn.sendCommandWithoutArgs(Command.Close);
                ((CurrentVideoInfo)getApplicationContext()).deleteFileInfo();
                Intent intent = new Intent(ControlActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });

        forward = (BootstrapButton) findViewById(R.id.forwardBtn);
        forward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rewind(Command.Forward.getId(), prefs.getInt("fast_rewind_interval_key", 10), 1);
            }
        });

        backward = (BootstrapButton) findViewById(R.id.backwardBtn);
        backward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rewind(Command.BackWard.getId(), prefs.getInt("fast_rewind_interval_key", 10), 1);
            }
        });

        fastForward = (BootstrapButton) findViewById(R.id.fastForwardBtn);
        fastForward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rewind(Command.Forward.getId(), prefs.getInt("fast_rewind_interval_key", 10), 60);
            }
        });

        fastBackward = (BootstrapButton) findViewById(R.id.fastBackwardBtn);
        fastBackward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rewind(Command.BackWard.getId(), prefs.getInt("fast_rewind_interval_key", 10), 60);
            }
        });

        muteBtn = (BootstrapButton) findViewById(R.id.muteBtn);
        muteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!((CurrentVideoInfo)getApplicationContext()).isFileInfoExist()) return;
                conn.sendCommandWithoutArgs(Command.Mute);
            }
        });

        refreshProgressBar();
        refreshTitle();
    }

    @Override
    protected void onStart() {
        super.onStart();
        changeColor();
        Intent myIntent = getIntent();

        if (((CurrentVideoInfo)getApplicationContext()).getSavedPosition() > 0) {
            AlertDialog.Builder builder = new AlertDialog.Builder(ControlActivity.this);
            builder.setMessage(getString(R.string.rewind_to_saved_pos))
                    .setPositiveButton(getString(R.string.yes), remoteToSavedPositionDialogListener)
                    .setNegativeButton(getString(R.string.no), setSavedPositionToZero).show();
        }
        if (((CurrentVideoInfo)getApplicationContext()).isFileInfoExist() && prefs.getBoolean("alwaysUpdate", false)) {
            Intent intent = new Intent(this, VideoInfoService.class);
            bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);
        }
    }

    private void refreshContent() {
        conn.sendCommandWithoutArgs(Command.Refresh);
        refreshProgressBar();
        refreshTitle();
        mSwipeRefreshLayout.setRefreshing(false);
    }

    private void rewind(final int commandId, final int value, final int multiple) {
        if (!((CurrentVideoInfo)getApplicationContext()).isFileInfoExist()) return;
        Thread t = new Thread(new Runnable(){
            @Override
            public void run() {
                Map<String, String> params = new HashMap<>();
                params.put("command", String.valueOf(commandId));
                params.put("seconds", String.valueOf(value * multiple));
                JSONObject result = conn.send(params);
            }
        });
        t.start();
        try {
            t.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (videoInfoService != null) {
            videoInfoService.startRequests();
        }
        refreshContent();
    }

    private void sendCommandChangeVolume(final String direction, final boolean isSys) {
        new Thread(new Runnable(){
            @Override
            public void run() {
                Map<String, String> params = new HashMap<>();
                params.put("command", String.valueOf(Command.ChangeVolume.getId()));
                params.put("direction", direction);
                params.put("isSys", String.valueOf(isSys));
                JSONObject result = conn.send(params);
            }
        }).start();
    }

    DialogInterface.OnClickListener closeAndDeleteDialogListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            conn.sendCommandWithoutArgs(Command.CloseDelete);
            ((CurrentVideoInfo)getApplicationContext()).deleteFileInfo();
            Intent intent = new Intent(ControlActivity.this, MainActivity.class);
            startActivity(intent);
        }
    };

    DialogInterface.OnClickListener remoteToSavedPositionDialogListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            Thread t = new Thread(new Runnable(){
                @Override
                public void run() {
                    Map<String, String> params = new HashMap<>();
                    params.put("command", String.valueOf(Command.SetPosition.getId()));
                    params.put("position", String.valueOf(((CurrentVideoInfo)getApplicationContext()).getSavedPosition()));
                    JSONObject result = conn.send(params);
                    // Сбрасываем сохраненную позицию, чтобы окно не появлялось вновь
                    ((CurrentVideoInfo)getApplicationContext()).setSavedPosition(0);
                }
            });
            t.start();
            try {
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            refreshProgressBar();
            refreshTitle();
        }
    };

    DialogInterface.OnClickListener setSavedPositionToZero = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {
            // Сбрасываем сохраненную позицию, чтобы окно не появлялось вновь
            ((CurrentVideoInfo) getApplicationContext()).setSavedPosition(0);
        }
    };

    private void refreshProgressBar() {
        if (!((CurrentVideoInfo)getApplicationContext()).isFileInfoExist()) return;
        float pos = Integer.parseInt(((CurrentVideoInfo)getApplicationContext()).getPosition());
        float duration = Integer.parseInt(((CurrentVideoInfo)getApplicationContext()).getDuration());
        if (duration != 0) {
            int percent = (int) ((pos / duration) * 100);
            pBar.setProgress(percent);
            pBar.invalidate();
        }

        curTime.setText(((CurrentVideoInfo)getApplicationContext()).getCurrentTime());
        totalTime.setText(((CurrentVideoInfo)getApplicationContext()).getTotalTime());
    }

    private void refreshTitle() {
        if (!((CurrentVideoInfo)getApplicationContext()).isFileInfoExist()) return;
        String fileName = ((CurrentVideoInfo)getApplicationContext()).getFilename();
        if (fileName != null && !fileName.isEmpty()) {
            if (fileName.length() > TITLE_LENGTH_LIMIT) {
                String cuttedFileName = fileName.substring(0, TITLE_LENGTH_LIMIT - 4);
                cuttedFileName += "~";
                cuttedFileName += fileName.substring(fileName.length()-4);
                fileName = cuttedFileName;
            }
            fileNameTV.setText(fileName);
        }
    }

    private void refreshPlayPauseBtn() {
        if (((CurrentVideoInfo)getApplicationContext()).isFileInfoExist()) {
            if ("2".equals(((CurrentVideoInfo)getApplicationContext()).getStatus())) {
                playPauseBtn.setImageDrawable(playImg);
            } else {
                playPauseBtn.setImageDrawable(pauseImg);
            }
        }
    }

    private void changeColor() {
        BootstrapBrand userColor = DefaultBootstrapBrand.valueOf(prefs.getString("pref_key_color_scheme", "BLUE"));
        int userColorInt = userColor.defaultFill(getApplicationContext());
        ActionBar bar = getSupportActionBar();
        if (bar != null) {
            bar.setBackgroundDrawable(new ColorDrawable(userColorInt));
        }
        fastBackward.setBootstrapBrand(userColor);
        backward.setBootstrapBrand(userColor);
        forward.setBootstrapBrand(userColor);
        fastForward.setBootstrapBrand(userColor);
        incVol.setBootstrapBrand(userColor);
        decVol.setBootstrapBrand(userColor);
        muteBtn.setBootstrapBrand(userColor);
        sound.setBootstrapBrand(userColor);
        fullScreen.setBootstrapBrand(userColor);
        closeAndDelete.setBootstrapBrand(userColor);
        closeAndSave.setBootstrapBrand(userColor);
        close.setBootstrapBrand(userColor);
        //sysVolumeCheckBox.setCircleColor(userColorInt);
        playPauseBtn.setBackgroundTintList(ColorStateList.valueOf(userColorInt));
        pBar.getIndeterminateDrawable().setColorFilter(userColorInt, PorterDuff.Mode.SRC_IN);
        pBar.getProgressDrawable().setColorFilter(userColorInt, PorterDuff.Mode.SRC_IN);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_in_control, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        Intent intent = null;
        switch (id) {
            case R.id.toMainScreen:
                intent = new Intent(ControlActivity.this, MainActivity.class);
                break;
            case R.id.prev_sub:
                conn.sendCommandWithoutArgs(Command.PrevSub);
                break;
            case R.id.next_sub:
                conn.sendCommandWithoutArgs(Command.NextSub);
                break;
            case R.id.prev_audio:
                conn.sendCommandWithoutArgs(Command.PrevAudio);
                break;
            case R.id.next_audio:
                conn.sendCommandWithoutArgs(Command.NextAudio);
                break;
            case R.id.action_settings:
                intent = new Intent(ControlActivity.this, SettingsActivity.class);
                break;
            case R.id.help_page:
                intent = new Intent(ControlActivity.this, AboutActivity.class);
                break;
        }
        if (intent != null) {
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onStop() {
        SingleToast.hide();
        super.onStop();
        // Unbind from service
        if (bound) {
            if (videoInfoService != null) {
                videoInfoService.setCallbacks(null); // unregister
            }
            unbindService(serviceConnection);
            bound = false;
        }
    }

    /** Callbacks for service binding, passed to bindService() */
    private ServiceConnection serviceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            // cast the IBinder and get MyService instance
            VideoInfoService.LocalBinder binder = (VideoInfoService.LocalBinder) service;
            videoInfoService = binder.getService();
            bound = true;
            videoInfoService.setCallbacks(ControlActivity.this); // register
            videoInfoService.startRequests();
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            bound = false;
            videoInfoService.stopRequests();
        }
    };


    @Override
    public void refreshProgress() {
        Handler refresh = new Handler(Looper.getMainLooper());
        refresh.post(new Runnable() {
            public void run()
            {
                refreshProgressBar();
                refreshTitle();
                refreshPlayPauseBtn();
            }
        });
    }

    @Override
    public void showError(final String message) {
        Handler shower = new Handler(Looper.getMainLooper());
        shower.post(new Runnable() {
            @Override
            public void run() {
                SingleToast.show(getApplicationContext(), message);
                Intent intent = new Intent(ControlActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });
    }
}
