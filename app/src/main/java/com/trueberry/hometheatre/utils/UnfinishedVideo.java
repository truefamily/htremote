package com.trueberry.hometheatre.utils;

import android.content.Context;

import com.trueberry.hometheatre.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Автор: root, дата: 18.08.2016.
 */
public class UnfinishedVideo extends Video {
    /* Контекст для доступа к ресурсам */
    private Context ctx;
    private String position;
    private String dateTime;

    public UnfinishedVideo(Context ctx, String fileName, String filePath, String dimension,
                           String filesize, String lastModifiedDate, String duration, String position, String dateTime) {
        super(fileName, filePath, dimension, filesize, lastModifiedDate, duration);
        this.position = position;
        this.dateTime = dateTime.substring(0, dateTime.indexOf('.'));
        this.ctx = ctx;
    }

    public String getConvertedPosition() {
        if (position == null) {
            return "";
        }
        int totalSecs = Integer.parseInt(position) / 1000;
        int minutes = totalSecs / 60;
        int seconds = totalSecs % 60;

        return String.format(ctx.getString(R.string.formatted_time_unfinished), minutes, seconds);
    }

    public String getConvertedDateTime() throws ParseException {
        SimpleDateFormat fromUser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        SimpleDateFormat myFormat = new SimpleDateFormat("dd.MM.yy HH:mm");
        return myFormat.format(fromUser.parse(this.dateTime));
    }
}
