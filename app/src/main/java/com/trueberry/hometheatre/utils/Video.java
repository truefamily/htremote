package com.trueberry.hometheatre.utils;

/**
 * Автор: root, дата: 18.08.2016.
 */
public class Video {
    protected String fileName;
    protected String filePath;
    protected String dimension;
    protected String filesize;
    protected String lastModifiedDate;
    protected String duration;

    public Video(String fileName, String filePath, String dimension, String filesize, String lastModifiedDate, String duration) {
        this.fileName = fileName;
        this.filePath = filePath;
        this.dimension = dimension;
        this.filesize = filesize;
        this.lastModifiedDate = lastModifiedDate;
        this.duration = duration;
    }

    public String getFileName() {
        return fileName;
    }

    public String getFilePath() {
        return filePath;
    }

    public String getDimension() {
        return dimension;
    }

    public String getFilesize() {
        return filesize;
    }

    public String getLastModifiedDate() {
        return lastModifiedDate;
    }

    public String getDuration() {
        return duration;
    }
}
