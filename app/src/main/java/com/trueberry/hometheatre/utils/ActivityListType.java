package com.trueberry.hometheatre.utils;

/**
 * Автор: root, дата: 18.08.2016.
 */
public enum ActivityListType {
    VIDEO_LIST,
    NOT_FINISHED_LIST,
    WATCHED_LIST
}
