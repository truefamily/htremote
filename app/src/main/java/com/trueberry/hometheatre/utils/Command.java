package com.trueberry.hometheatre.utils;

/**
 * Created by root on 08.08.2015.
 */
public enum Command {

    Sound(1),
    SeriesList(2),
    MoviesList(3),
    Settings(4),
    Run(5),
    PlayPause(6),
    Close(7),
    CloseDelete(8),
    CloseSave(9),
    Stop(10),
    IncVolume(11),
    DecVolume(12),
    FullScreen(13),
    Forward(14),
    BackWard(15),
    Refresh(16),
    ChangeVolume(17),
    Mute(18),
    CloseServer(19),
    TurnOffPC(20),
    SetPosition(21),
    PrevSub(22),
    NextSub(23),
    PrevAudio(24),
    NextAudio(25),
    PlayMovie(26),
    PauseMovie(27),
    NotFinishedList(28),
    WatchedList(29),
    DeleteVideo(30);

    Command(int id) {
        this.id = id;
    }

    private int id;

    public int getId() {
        return id;
    }
}
