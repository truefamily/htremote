package com.trueberry.hometheatre.utils;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.BaseSwipeAdapter;
import com.trueberry.hometheatre.Connector;
import com.trueberry.hometheatre.R;
import com.trueberry.hometheatre.helpers.SingleToast;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Автор: root, дата: 18.08.2016.
 */
public class VideoAdapter extends BaseSwipeAdapter {

    private Context mContext;
    private List<Video> myList;
    private Connector connector;
    private static int swipeResId = R.id.swipe_video_layout;

    public VideoAdapter(Context mContext, List<Video> myList) {
        this.mContext = mContext;
        this.myList = myList;
        connector = new Connector(mContext);
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return swipeResId;
    }

    @Override
    public View generateView(int position, ViewGroup parent) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.list_view_video, null);
        SwipeLayout swipeLayout = (SwipeLayout) v.findViewById(swipeResId);
        // Возможность закрытия доп. действия кликом на верхний view
        swipeLayout.setClickToClose(true);
        //set show mode.
        swipeLayout.setShowMode(SwipeLayout.ShowMode.PullOut);

        //add drag edge.(If the BottomView has 'layout_gravity' attribute, this line is unnecessary)
        //swipeLayout.addDrag(SwipeLayout.DragEdge.Left, findViewById(R.id.bottom_wrapper));

        swipeLayout.addSwipeListener(new SwipeLayout.SwipeListener() {
            @Override
            public void onClose(SwipeLayout layout) {
                //when the SurfaceView totally cover the BottomView.

            }

            @Override
            public void onUpdate(SwipeLayout layout, int leftOffset, int topOffset) {
                //you are swiping.
            }

            @Override
            public void onStartOpen(SwipeLayout layout) {

            }

            @Override
            public void onOpen(SwipeLayout layout) {
                //when the BottomView totally show.
            }

            @Override
            public void onStartClose(SwipeLayout layout) {

            }

            @Override
            public void onHandRelease(SwipeLayout layout, float xvel, float yvel) {
                //when user's hand released.
            }
        });
        return v;

    }

    @Override
    public void fillValues(final int position, final View convertView) {
        final Video item = myList.get(position);
        TextView fileNameView = (TextView) convertView.findViewById(R.id.video_filename);
        if (fileNameView != null) {
            fileNameView.setText(item.getFileName());
        }
        convertView.findViewById(R.id.delete_video).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Video item = getItem(position);
                AlertDialog.Builder deleteVideoBuilder = new AlertDialog.Builder(mContext);
                deleteVideoBuilder.setMessage(mContext.getString(R.string.delete_current_video))
                        .setPositiveButton(mContext.getString(R.string.yes), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                deleteVideoListener(item);
                            }
                        })
                        .setNegativeButton(mContext.getString(R.string.no), null).show();
            }
        });
    }

    @Override
    public int getCount()
    {
        return myList.size();
    }

    @Override
    public Video getItem(int position)
    {
        return myList.get(position);
    }

    @Override
    public long getItemId(int position)
    {
        return position;
    }

    private void deleteVideoListener(final Video video) {
        Thread t = new Thread(new Runnable(){
            @Override
            public void run() {
                Map<String, String> params = new HashMap<>();
                params.put("command", String.valueOf(Command.DeleteVideo.getId()));
                params.put("filepath", String.valueOf(video.getFilePath()));
                params.put("filename", String.valueOf(video.getFileName()));
                JSONObject result = connector.send(params);

                Intent intent = new Intent(mContext, mContext.getClass());
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                mContext.startActivity(intent);
                SingleToast.show(mContext, mContext.getString(R.string.message_file_deleted));
            }
        });
        t.start();
        try {
            t.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}