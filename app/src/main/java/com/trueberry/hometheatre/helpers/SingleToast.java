package com.trueberry.hometheatre.helpers;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.widget.Toast;

/**
 * Автор: root, дата: 09.06.2016.
 */
public class SingleToast {

    private static Toast mToast;

    public static void show(Context context, String text) {
        show(context, text, Toast.LENGTH_SHORT);
    }

    public static void show(final Context context, final String text, final int duration) {
        Handler shower = new Handler(Looper.getMainLooper());
        shower.post(new Runnable() {
            @Override
            public void run() {
                if (mToast != null) {
                    mToast.cancel();
                }
                mToast = Toast.makeText(context, text, duration);
                mToast.show();
            }
        });
    }

    public static void hide() {
        if (mToast != null) {
            mToast.cancel();
        }
    }

    public static Toast getToast() {
        return mToast;
    }


}