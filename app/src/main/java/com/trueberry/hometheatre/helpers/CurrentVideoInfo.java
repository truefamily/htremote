package com.trueberry.hometheatre.helpers;

import android.app.Application;

import java.util.HashMap;

/**
 * Автор: root, дата: 09.07.2016.
 */
public class CurrentVideoInfo extends Application {

    // Информация о текущем проигрываемом видеофайле
    private HashMap<String, String> fileInfo;
    // Тип видеофайла: сериал, фильм, ..
    private String mediaType;
    // Сохраненная позиция воспроизведения
    private int savedPosition;

    public String getMediaType() {
        return mediaType;
    }

    public void setMediaType(String mediaType) {
        this.mediaType = mediaType;
    }

    public int getSavedPosition() {
        return savedPosition;
    }

    public void setSavedPosition(int savedPosition) {
        this.savedPosition = savedPosition;
    }

    public void setFileInfo(HashMap<String, String> fileInfo) {
        this.fileInfo = fileInfo;
    }

    public HashMap<String, String> getFileInfo() {
        return fileInfo;
    }

    public String getStatus() {
        if (isFileInfoExist()) {
            return fileInfo.get("Status");
        }
        return null;
    }

    public String getPosition() {
        if (isFileInfoExist()) {
            return fileInfo.get("Position");
        }
        return null;
    }

    public String getDuration() {
        if (isFileInfoExist()) {
            return fileInfo.get("Duration");
        }
        return null;
    }

    public String getFilename() {
        if (isFileInfoExist()) {
            return fileInfo.get("FileName");
        }
        return null;
    }

    public String getCurrentTime() {
        if (isFileInfoExist()) {
            return fileInfo.get("CurrentTime");
        }
        return null;
    }

    public String getTotalTime() {
        if (isFileInfoExist()) {
            return fileInfo.get("TotalTime");
        }
        return null;
    }

    public boolean isFileInfoExist() {
        return fileInfo != null;
    }

    public void deleteFileInfo() {
        fileInfo = null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        // При запуске приложения обнуляем информацию о видео
        fileInfo = null;
    }
}
