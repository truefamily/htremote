package com.trueberry.hometheatre.helpers;

import android.content.Context;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.trueberry.hometheatre.utils.UnfinishedVideo;
import com.trueberry.hometheatre.utils.Video;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Автор: root, дата: 20.11.2015.
 */
public class JSONHelper {

    public static List<UnfinishedVideo> getUnfinishedVideoListFromJSON(Context ctx, JSONObject json) throws JSONException {
        JSONArray arr = json.getJSONArray("list");
        List<UnfinishedVideo> unfinishedVideos = new ArrayList<>();
        for (int i = 0; i < arr.length(); i++) {
            JSONObject o = (JSONObject)arr.get(i);
            UnfinishedVideo item = new UnfinishedVideo(ctx, o.getString("filename"),o.getString("filepath"),
                    null, null, null, null, o.getString("position"),o.getString("timestamp"));
            unfinishedVideos.add(item);
        }
        return unfinishedVideos;
    }

    public static List<Video> getVideoListFromJSON(JSONObject json) throws JSONException {
        JSONArray arr = json.getJSONArray("list");
        List<Video> videos = new ArrayList<>();
        for (int i = 0; i < arr.length(); i++) {
            JSONObject o = (JSONObject)arr.get(i);
            Video item = new Video(
                    o.getString("filename"), o.getString("filepath"),
                    o.getString("dimension"), o.getString("filesize"),
                    o.getString("lastmodifieddate"), o.getString("duration")
            );
            videos.add(item);
        }
        return videos;
    }

    public static HashMap<String, String> getJsonAsMap(JSONObject json)
    {
        try
        {
            ObjectMapper mapper = new ObjectMapper();
            TypeReference<Map<String,String>> typeRef = new TypeReference<Map<String,String>>() {};
            return mapper.readValue(json.toString(), typeRef);
        }
        catch (Exception e)
        {
            throw new RuntimeException("Couldnt parse json:" + json, e);
        }
    }
}
