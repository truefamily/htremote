package com.trueberry.hometheatre;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.preference.PreferenceManager;

import com.trueberry.hometheatre.helpers.CurrentVideoInfo;
import com.trueberry.hometheatre.helpers.JSONHelper;
import com.trueberry.hometheatre.helpers.SingleToast;
import com.trueberry.hometheatre.utils.Command;

import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.entity.UrlEncodedFormEntity;
import cz.msebera.android.httpclient.client.methods.HttpPost;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;
import cz.msebera.android.httpclient.message.BasicNameValuePair;
import cz.msebera.android.httpclient.params.BasicHttpParams;
import cz.msebera.android.httpclient.params.HttpConnectionParams;
import cz.msebera.android.httpclient.params.HttpParams;

/**
 * Автор: root, дата: 02.08.2015.
 */
public class Connector {

    private SharedPreferences prefs = null;
    private static Context ctx;

    public Connector(Context ctx) {
        Connector.ctx = ctx;
        prefs = PreferenceManager.getDefaultSharedPreferences(ctx);
    }

    public JSONObject send(Command command) {
        Map<String, String> params = new HashMap<>();
        params.put("command", String.valueOf(command.getId()));
        return send(params);
    }

    public JSONObject send(Map<String, String> params) {
        InputStream in = null;
        try {
            // Добавляем пароль в запрос
            params.put("password", String.valueOf(prefs.getString("pass_key_pref", null)));

            // TODO Переделать, с 6.0 не так просто?!
            WifiManager wifiManager = (WifiManager) ctx.getSystemService(Context.WIFI_SERVICE);
            if (!wifiManager.isWifiEnabled()) {
                throw new Exception(ctx.getString(R.string.is_wifi_enabled));
            }

            WifiInfo wInfo = wifiManager.getConnectionInfo();
            String macAddress = wInfo.getMacAddress();

            // Если mac'a нет, значит, сеть не включена
            if (macAddress == null) {
                throw new Exception(ctx.getString(R.string.is_wifi_enabled));
            }
            // Добавляем идентификатор устройства в запрос
            params.put("identifier", macAddress);

            String ipAddress = prefs.getString("ip_key_pref", "");
            int port = Integer.valueOf(prefs.getString("port_key_pref", "4444"));
            if (ipAddress.isEmpty()) {
                throw new Exception(ctx.getString(R.string.common_error));
            }

            List<NameValuePair> postParams = new ArrayList<>(params.size());
            for (Map.Entry<String, String> entry : params.entrySet())
            {
                postParams.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
            }
            // Настраиваем таймаут установления соединения в мс.
            int timeoutConnection = 10000;
            // Настраиваем socket timeout (SO_TIMEOUT) в мс, которые ожидаются данные.
            int timeoutSocket = 30000;
            HttpParams httpParameters = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
            HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
            HttpClient httpclient = new DefaultHttpClient(httpParameters);
            HttpPost httppost = new HttpPost("http://" + ipAddress + ":" + port);
            httppost.setEntity(new UrlEncodedFormEntity(postParams, "UTF-8"));

            HttpResponse response = httpclient.execute(httppost);
            HttpEntity entity = response.getEntity();
            in = new BufferedInputStream(entity.getContent());
            String result = convertStreamToString(in);
            if (result == null || result.isEmpty()) {
                throw new Exception(ctx.getString(R.string.answer_is_empty));
            }
            JSONObject jsonResult = new JSONObject(result);
            if (jsonResult.has("error")) {
                throw new Exception(jsonResult.getString("error"));
            }
            if (jsonResult.has("status")) {
                if (!"OK".equals(jsonResult.getString("status"))) {
                    throw new Exception(ctx.getString(R.string.common_error));
                }
            }
            if (jsonResult.has("Info")) {
                ((CurrentVideoInfo) ctx.getApplicationContext()).setFileInfo(JSONHelper.getJsonAsMap(jsonResult.getJSONObject("Info")));
            }
            return jsonResult;
        } catch (IOException ioe) {
            SingleToast.show(ctx, ctx.getString(R.string.not_connected_to_server));
        } catch (Exception e) {
            SingleToast.show(ctx, e.getMessage());
        } finally {
            try { if (in != null) in.close(); } catch(IOException ignored) {}
        }
        return null;
    }

    private String convertStreamToString(java.io.InputStream is) {
        java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next() : null;
    }

    public void sendCommandWithoutArgs(final Command command) {
        Thread t = new Thread(new Runnable(){
            @Override
            public void run() {
                send(command);
            }
        });
        t.start();
        try {
            t.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
