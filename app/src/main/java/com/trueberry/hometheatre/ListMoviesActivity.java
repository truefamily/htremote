package com.trueberry.hometheatre;

import com.trueberry.hometheatre.utils.ActivityListType;
import com.trueberry.hometheatre.utils.Command;

/**
 * Created by root on 06.08.2015.
 * Список фильмов.
 */
public class ListMoviesActivity extends ListActivity {

    public ListMoviesActivity() {
        getListCommand = Command.MoviesList;
        type = "movies";
        listType = ActivityListType.VIDEO_LIST;
    }
}
