package com.trueberry.hometheatre;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.beardedhen.androidbootstrap.BootstrapButton;
import com.beardedhen.androidbootstrap.TypefaceProvider;
import com.beardedhen.androidbootstrap.api.attributes.BootstrapBrand;
import com.beardedhen.androidbootstrap.api.defaults.DefaultBootstrapBrand;
import com.kobakei.ratethisapp.RateThisApp;
import com.trueberry.hometheatre.helpers.SingleToast;
import com.trueberry.hometheatre.receivers.PhoneStateService;
import com.trueberry.hometheatre.utils.Command;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    private BootstrapButton controlBtn, seriesBtn, moviesBtn, closeServerBtn, turnOffPCBtn,
            notFinishedBtn;
    private List<BootstrapButton> activityButtonsList;
    private SharedPreferences prefs = null;
    private Connector conn = null;
    private boolean phoneStateBound = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TypefaceProvider.registerDefaultIconSets();
        prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        // устанавливаем текующую локаль, заданную в настройках
        changeLocale();
        setContentView(R.layout.activity_main);

        // RateIt dialog block
        RateThisApp.Config config = new RateThisApp.Config(3, 5);
        config.setTitle(R.string.rate_title);
        config.setMessage(R.string.rate_text);
        config.setYesButtonText(R.string.rate_it);
        config.setNoButtonText(R.string.rate_no);
        config.setCancelButtonText(R.string.rate_later);
        RateThisApp.init(config);

        conn = new Connector(getApplicationContext());

        if (prefs.getBoolean("auto_switch_key", false)) {
            conn.sendCommandWithoutArgs(Command.Sound);
            SingleToast.show(getApplicationContext(), getString(R.string.alternative_audio));
        }

        // Получаем сущности контролов
        controlBtn = (BootstrapButton) findViewById(R.id.controlBtn);
        seriesBtn = (BootstrapButton) findViewById(R.id.seriesBtn);
        moviesBtn = (BootstrapButton) findViewById(R.id.moviesBtn);
        closeServerBtn = (BootstrapButton) findViewById(R.id.quitBtn);
        turnOffPCBtn = (BootstrapButton) findViewById(R.id.shutdownBtn);
        notFinishedBtn = (BootstrapButton) findViewById(R.id.notFinishedBtn);

        // Заполняем массив всех кнопок активити, чтобы потом разом их менять
        activityButtonsList = Arrays.asList(controlBtn, seriesBtn, moviesBtn, closeServerBtn,
                turnOffPCBtn, notFinishedBtn);

        // Настраиваем обработчики кликов по основным кнопкам.
        controlBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ControlActivity.class);
                startActivity(intent);
            }
        });
        seriesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ListSeriesActivity.class);
                startActivity(intent);
            }
        });
        moviesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ListMoviesActivity.class);
                startActivity(intent);
            }
        });
        notFinishedBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ListUnfinishedActivity.class);
                startActivity(intent);
            }
        });

        closeServerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                conn.sendCommandWithoutArgs(Command.CloseServer);
            }
        });

        turnOffPCBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                conn.sendCommandWithoutArgs(Command.TurnOffPC);
            }
        });

        if (!phoneStateBound) {
            Intent intent = new Intent(this, PhoneStateService.class);
            bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);
            phoneStateBound = true;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        changeColor();
        // Monitor launch times and interval from installation
        RateThisApp.onStart(this);
        // If the criteria is satisfied, "Rate this app" dialog will be shown
        RateThisApp.showRateDialogIfNeeded(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_in_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
            startActivity(intent);
        }
        if (id == R.id.help_page) {
            Intent intent = new Intent(MainActivity.this, AboutActivity.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed(){
        Toast toast = SingleToast.getToast();
        // TODO Отрефакторить индусский код
        if(toast != null && toast.getView().getWindowToken() != null) {
            String text = ((TextView)((LinearLayout)toast.getView()).getChildAt(0)).getText().toString();
            if (text.equals(getString(R.string.press_back_to_exit))) {
                SingleToast.hide();
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            } else {
                SingleToast.show(getApplicationContext(), getString(R.string.press_back_to_exit));
            }
        } else {
            SingleToast.show(getApplicationContext(), getString(R.string.press_back_to_exit));
        }
    }

    @Override
    public void onDestroy() {
        // Unbind from service
        if (phoneStateBound) {
            unbindService(serviceConnection);
            phoneStateBound = false;
        }
        super.onDestroy();
    }

    private void changeColor() {
        BootstrapBrand userColor = DefaultBootstrapBrand.valueOf(prefs.getString("pref_key_color_scheme", "BLUE"));
        ActionBar bar = getSupportActionBar();
        if (bar != null) {
            bar.setBackgroundDrawable(new ColorDrawable(userColor.defaultFill(getApplicationContext())));
        }
        for (BootstrapButton btn : activityButtonsList) {
            if (btn == null) continue;
            btn.setBootstrapBrand(userColor);
        }
    }

    private void changeLocale() {
        String prefsLocale = prefs.getString("pref_key_language", "default");
        if ("default".equals(prefsLocale)) {
            prefsLocale = Resources.getSystem().getConfiguration().locale.getLanguage();
        }
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = new Locale(prefsLocale);
        res.updateConfiguration(conf, dm);
    }

    /** Callbacks for service binding, passed to bindService() */
    private ServiceConnection serviceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            phoneStateBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            phoneStateBound = false;
        }
    };

}
