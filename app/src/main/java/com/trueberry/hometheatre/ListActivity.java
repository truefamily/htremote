package com.trueberry.hometheatre;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.beardedhen.androidbootstrap.api.attributes.BootstrapBrand;
import com.beardedhen.androidbootstrap.api.defaults.DefaultBootstrapBrand;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.trueberry.hometheatre.helpers.CurrentVideoInfo;
import com.trueberry.hometheatre.helpers.JSONHelper;
import com.trueberry.hometheatre.helpers.SingleToast;
import com.trueberry.hometheatre.utils.ActivityListType;
import com.trueberry.hometheatre.utils.Command;
import com.trueberry.hometheatre.utils.UnfinishedAdapter;
import com.trueberry.hometheatre.utils.UnfinishedVideo;
import com.trueberry.hometheatre.utils.Video;
import com.trueberry.hometheatre.utils.VideoAdapter;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by root on 06.08.2015.
 * Общий класс для списка видеофайлов.
 */
public abstract class ListActivity extends AppCompatActivity {

    // Команда для получения файлов
    protected Command getListCommand;
    // Тип файлов в списке
    protected String type;
    // Тип активити
    protected ActivityListType listType;

    private Connector connection;
    private ListView listView;
    private ProgressWheel progressWheel;
    private SharedPreferences prefs = null;
    private Dialog mOverlayDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_activity);

        prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        connection = new Connector(getApplicationContext());
        listView = (ListView) findViewById(R.id.list_view);
        progressWheel = (ProgressWheel) findViewById(R.id.marker_progress);
        mOverlayDialog = new Dialog(ListActivity.this, android.R.style.Theme_Panel);
        mOverlayDialog.setCancelable(false);

        // Асинхронно получаем список файлов
        new Thread(getListFromServer()).start();
    }

    @Override
    protected void onStart() {
        super.onStart();
        changeColor();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_in_lists, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.help_page) {
            Intent intent = new Intent(ListActivity.this, AboutActivity.class);
            startActivity(intent);
        }
        if (id == R.id.toMainScreen) {
            Intent intent = new Intent(ListActivity.this, MainActivity.class);
            startActivity(intent);
        }
        if (id == R.id.toSettings) {
            Intent intent = new Intent(ListActivity.this, SettingsActivity.class);
            startActivity(intent);
        }
        if (id == R.id.toControl) {
            Intent intent = new Intent(ListActivity.this, ControlActivity.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    private void spinProgressWheel() {
        if(progressWheel != null) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progressWheel.spin();
                }
            });
        }
    }

    private void stopProgressWheel() {
        if (progressWheel != null) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progressWheel.stopSpinning();
                }
            });
        }
    }

    /**
     * Получить список файлов с сервера
     * @return Runnable поток выполнения запроса к серверу на получение списка файлов
     */
    public Runnable getListFromServer() {
        // Запускаем троббер, выключаем либо по таймауту, либо когда загрузится список
        spinProgressWheel();

        return new Runnable() {
            @Override
            public void run() {
                try {
                    final JSONObject result;
                    Map<String, String> params = new HashMap<>();
                    final ListAdapter listAdapter;

                    params.put("command", String.valueOf(getListCommand.getId()));
                    if (listType == ActivityListType.VIDEO_LIST) {
                        params.put("recursively", String.valueOf(prefs.getBoolean("recursively", false)));
                    }
                    result = connection.send(params);
                    switch (listType) {
                        case VIDEO_LIST:
                            final List<Video> videos = JSONHelper.getVideoListFromJSON(result);
                            listAdapter = new VideoAdapter(ListActivity.this, videos);
                            break;
                        case NOT_FINISHED_LIST:
                            final List<UnfinishedVideo> unfinishedVideos = JSONHelper.getUnfinishedVideoListFromJSON(getApplicationContext(), result);
                            listAdapter = new UnfinishedAdapter(ListActivity.this, unfinishedVideos);
                            break;
                        case WATCHED_LIST:
                        default:
                            throw new Exception(getString(R.string.unsupported_list_type_error));
                    }
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            listView.setAdapter(listAdapter);
                        }
                    });

                } catch (JSONException e) {
                    redirectToMainWithToast(getString(R.string.incorrect_server_response));
                } catch (final Exception e) {
                    redirectToMainWithToast(getString(R.string.not_connected_to_server));
                } finally {
                    stopProgressWheel();
                }

                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        // Блокируем взимодействие, пока не загрузится фильм
                        mOverlayDialog.show();

                        final Object selectedItem;
                        switch (listType) {
                            case VIDEO_LIST:
                            case NOT_FINISHED_LIST:
                                selectedItem = listView.getAdapter().getItem(position);
                                break;
                            default:
                                selectedItem = null;
                                break;
                        }

                        // Асинхронно запускаем плеер с выбранным видео на сервере
                        new Thread(runVideoOnTheServer(selectedItem)).start();
                    }
                });
                listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                    @Override
                    public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                        Video selectedItem = (Video)listView.getAdapter().getItem(i);
                        AlertDialog.Builder builder = new AlertDialog.Builder(ListActivity.this);
                        view = getLayoutInflater().inflate(R.layout.video_detail_view, null);

                        TextView filePathView = (TextView) view.findViewById(R.id.detail_filepath);
                        filePathView.setText(selectedItem.getFilePath());
                        TextView fileSizeView = (TextView) view.findViewById(R.id.detail_filesize);
                        fileSizeView.setText(selectedItem.getFilesize());
                        TextView dimensionView = (TextView) view.findViewById(R.id.detail_dimension);
                        dimensionView.setText(selectedItem.getDimension());
                        TextView durationView = (TextView) view.findViewById(R.id.detail_duration);
                        durationView.setText(selectedItem.getDuration());
                        TextView lastModifiedDateView = (TextView) view.findViewById(R.id.detail_lastmodifieddate);
                        lastModifiedDateView.setText(selectedItem.getLastModifiedDate());

                        builder.setView(view);
                        builder.setTitle(selectedItem.getFileName());
                        AlertDialog dialog = builder.create();

                        dialog.show();
                        return false;
                    }
                });
            }
        };
    }

    /**
     * Запустить видео на сервере
     * @param selectedItem выбранное видео
     * @return Runnable поток выполнения запроса к серверу
     */
    private Runnable runVideoOnTheServer(final Object selectedItem) {
        // Запускаем троббер, выключаем либо по таймауту, либо когда загрузится список
        spinProgressWheel();

        return new Runnable() {
            @Override
            public void run() {
                try {
                    Map<String, String> params = new HashMap<>();
                    params.put("command", String.valueOf(Command.Run.getId()));
                    params.put("filename", ((Video)selectedItem).getFileName());
                    switch (listType) {
                        case VIDEO_LIST:
                            params.put("recursively", String.valueOf(prefs.getBoolean("recursively", false)));
                            params.put("type", type);
                            break;
                        case NOT_FINISHED_LIST:
                            params.put("filepath", ((UnfinishedVideo)selectedItem).getFilePath());
                            break;
                        default:
                            break;
                    }
                    JSONObject result = connection.send(params);
                    if (result.has("Position")) {
                        ((CurrentVideoInfo)getApplicationContext()).setSavedPosition(result.getInt("Position"));
                    }
                    ((CurrentVideoInfo)getApplicationContext()).setMediaType(type);

                    Intent intent = new Intent(ListActivity.this, ControlActivity.class);
                    startActivity(intent);
                } catch (JSONException e) {
                    redirectToMainWithToast(e.getMessage());
                } finally {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mOverlayDialog.hide();
                            stopProgressWheel();
                        }
                    });
                }
            }
        };
    }

    private void redirectToMainWithToast(final String toastMessage) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                SingleToast.show(getApplicationContext(), toastMessage);
                Intent intent = new Intent(ListActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });
    }

    private void changeColor() {
        BootstrapBrand userColor = DefaultBootstrapBrand.valueOf(prefs.getString("pref_key_color_scheme", "BLUE"));
        ActionBar bar = getSupportActionBar();
        ColorDrawable colorDrawable = new ColorDrawable(userColor.defaultFill(getApplicationContext()));
        if (bar != null) {
            bar.setBackgroundDrawable(colorDrawable);
        }
        listView.setSelector(colorDrawable);
        progressWheel.setBarColor(userColor.defaultFill(getApplicationContext()));
    }
}