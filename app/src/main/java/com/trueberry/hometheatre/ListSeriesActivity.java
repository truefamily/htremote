package com.trueberry.hometheatre;

import com.trueberry.hometheatre.utils.ActivityListType;
import com.trueberry.hometheatre.utils.Command;

/**
 * Created by root on 06.08.2015.
 * Список сериалов.
 */
public class ListSeriesActivity extends ListActivity {

    public ListSeriesActivity() {
        getListCommand = Command.SeriesList;
        type = "series";
        listType = ActivityListType.VIDEO_LIST;
    }
}
