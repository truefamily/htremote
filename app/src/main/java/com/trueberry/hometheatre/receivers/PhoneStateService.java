package com.trueberry.hometheatre.receivers;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Binder;
import android.os.IBinder;

import com.trueberry.hometheatre.Connector;

/**
 * Автор: root, дата: 27.06.2016.
 */
public class PhoneStateService extends Service {
    // Binder, отдается клиентам
    private final IBinder binder = new LocalBinder();
    // Коннектор для взаиомдействия с сервером
    Connector connector;
    // Мой Broadcast receiver для получения сообщений о состоянии телефона
    private final BroadcastReceiver myBroadcast = new PhoneStateReceiver();

    public class LocalBinder extends Binder {
        public PhoneStateService getService() {
            return PhoneStateService.this;
        }
    }

    public void onCreate() {
        super.onCreate();
        connector = new Connector(getApplicationContext());
        IntentFilter filter = new IntentFilter();
        filter.addAction("android.intent.action.PHONE_STATE");
        registerReceiver(myBroadcast, filter);
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(myBroadcast);
    }

    public IBinder onBind(Intent intent) {
        return binder;
    }

}
