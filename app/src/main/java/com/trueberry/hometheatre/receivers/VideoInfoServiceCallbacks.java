package com.trueberry.hometheatre.receivers;

/**
 * Автор: root, дата: 27.06.2016.
 */
public interface VideoInfoServiceCallbacks {
    // Обновить прогресс воспроизведения
    void refreshProgress();
    // Показать ошибку
    void showError(String message);
}