package com.trueberry.hometheatre.receivers;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;

import com.trueberry.hometheatre.Connector;
import com.trueberry.hometheatre.helpers.CurrentVideoInfo;
import com.trueberry.hometheatre.utils.Command;

import org.json.JSONObject;

/**
 * Автор: root, дата: 27.06.2016.
 */
public class VideoInfoService extends Service {
    // Binder, отдается клиентам
    private final IBinder binder = new LocalBinder();
    // Зарегистрированные события
    private VideoInfoServiceCallbacks videoInfoServiceCallbacks;
    // Коннектор для взаиомдействия с сервером
    Connector connector;
    // Хэндлер в роли таймера
    private final Handler mHandler = new Handler();
    // Флаг остановки запросов
    private boolean stopped = true;
    // Идентификтаор для логов
    final String LOG_TAG = "VideoInfoService";

    public class LocalBinder extends Binder {
        public VideoInfoService getService() {
            return VideoInfoService.this;
        }
    }

    public void onCreate() {
        super.onCreate();
        connector = new Connector(getApplicationContext());
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    public void onDestroy() {
        super.onDestroy();
    }

    public IBinder onBind(Intent intent) {
        return binder;
    }

    public void setCallbacks(VideoInfoServiceCallbacks callbacks) {
        videoInfoServiceCallbacks = callbacks;
    }

    void refreshFileInfo() {
        try {
            JSONObject result = connector.send(Command.Refresh);
            if (videoInfoServiceCallbacks != null) {
                if (result.has("error")) {
                    videoInfoServiceCallbacks.showError((String) result.get("error"));
                    stopped = true;
                    ((CurrentVideoInfo)getApplicationContext()).deleteFileInfo();
                    return;
                }
                videoInfoServiceCallbacks.refreshProgress();
                // Через 1 секунду повторяем запрос
                mHandler.postDelayed(mUpdateClock, 1000);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void startRequests() {
        if (stopped) {
            stopped = false;
            mHandler.post(mUpdateClock);
        }
    }

    public void stopRequests() {
        stopped = true;
    }

    private final Runnable mUpdateClock = new Runnable() {
        public void run() {
            if(isNeedAnotherTick()) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        // Запрашиваем актуальные данные с сервера
                        refreshFileInfo();
                    }
                }).start();
            }
        }
    };

    private boolean isNeedAnotherTick() {
        return !stopped &&
                videoInfoServiceCallbacks != null &&
                ((CurrentVideoInfo)getApplicationContext()).isFileInfoExist();
    }
}
