package com.trueberry.hometheatre.receivers;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.trueberry.hometheatre.utils.Command;
import com.trueberry.hometheatre.Connector;
import com.trueberry.hometheatre.helpers.CurrentVideoInfo;

public class MyPhoneStateListener extends PhoneStateListener {
    // Контекст
    Context context;
    // Флаг входящего звонка
    static boolean wasCall = false;
    // Настройки приложения
    private SharedPreferences prefs = null;
    // Коннектор
    Connector connector;

    public MyPhoneStateListener(Context context) {
        this.context = context;
        prefs = PreferenceManager.getDefaultSharedPreferences(context);
        connector = new Connector(context);
    }

    @Override
    public void onCallStateChanged(int state,String incomingNumber){
        boolean isPauseWhenCalling = prefs.getBoolean("pause_when_calling_key", false);
        if (!isPauseWhenCalling) {
            return;
        }
        if(!((CurrentVideoInfo)context.getApplicationContext()).isFileInfoExist()) {
            return;
        }
        switch(state){
            case TelephonyManager.CALL_STATE_IDLE:
                Log.d("DEBUG", "IDLE");
                if (wasCall) {
                    // TODO Сделать только Play
                    connector.sendCommandWithoutArgs(Command.PlayPause);
                    wasCall = false;
                }
                break;
            case TelephonyManager.CALL_STATE_OFFHOOK:
                Log.d("DEBUG", "OFFHOOK");
                break;
            case TelephonyManager.CALL_STATE_RINGING:
                Log.d("DEBUG", "RINGING");
                // Бывает, что ресивер получает несколько сообщений о звонке подряд,
                // И нам не нужно посылать несколько раз паузу серверу.
                if (!wasCall) {
                    // TODO Сделать только Pause
                    connector.sendCommandWithoutArgs(Command.PlayPause);
                    // Устанавливаем флаг того, что был звонок, чтобы нажимать паузу только тогда,
                    // когда нам звонили во время просмотра
                    wasCall = true;
                }
        }
    }
}